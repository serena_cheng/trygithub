//
//  OrganizationViewController.m
//  TestGithub
//
//  Created by serena_cheng on 10/21/15.
//  Copyright (c) 2015 serena_cheng. All rights reserved.
//

#import "OrganizationViewController.h"
#import "RepositoriesTableViewController.h"

@interface OrganizationViewController ()

@end

@implementation OrganizationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"showRepos"]) {
        ((RepositoriesTableViewController *)[segue destinationViewController]).organization = self.organizationTextField.text;
    }
}

- (IBAction)clickedButton:(id)sender {
    
    NSString *organ = self.organizationTextField.text;
    if(organ== nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please fill out the organization." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        [alert show];
    } else {
        [self performSegueWithIdentifier:@"showRepos" sender:self];
    }
}


@end
