//
//  main.m
//  TestGithub
//
//  Created by serena_cheng on 10/21/15.
//  Copyright (c) 2015 serena_cheng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
