//
//  ActivitiesViewController.m
//  TestGithub
//
//  Created by serena_cheng on 10/21/15.
//  Copyright (c) 2015 serena_cheng. All rights reserved.
//

#import "ActivitiesViewController.h"

#define GITHUB_REPO_EVENT_API @"https://api.github.com/repos/%@/%@/events"

@interface ActivitiesViewController () 

@property (nonatomic, strong) NSArray *events;

@end

@implementation ActivitiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Events";
    
    self.calendarView.delegate = self;
    self.calendarView.dataSource = self;
    self.calendarView.showsEvents = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self getEventsFromRepo];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self stepMonthInCalendarByValue:24 animation:NO];
    [self calendarController:self.calendarView didScrollToMonth:self.calendarView.monthDisplayed];
}

- (void)getEventsFromRepo {

    NSString *owner = self.repoInfo[@"owner"][@"login"];
    NSString *name = self.repoInfo[@"name"];
    NSString *api = [NSString stringWithFormat:GITHUB_REPO_EVENT_API, owner, name];
    
    NSURL *url = [NSURL URLWithString:api];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20];
    
    urlRequest.HTTPMethod = @"Get";
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        NSInteger responseStatusCode = [httpResponse statusCode];
        NSLog(@"Status Code: %ld", (long)responseStatusCode);
        
        if([data length] > 0 && connectionError == nil && responseStatusCode == 200) {
            self.events = [NSJSONSerialization JSONObjectWithData:data
                                                         options:0
                                                           error:nil];
            
            self.calendarView.activities = self.events;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.calendarView.collectionView reloadData];
            });
            
        } else if([data length] == 0 && connectionError == nil) {
            NSLog(@"No data.");
        } else if(connectionError != nil) {
            NSLog(@"Connection failed.");
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - KDCalendarDataSource

- (NSDate*)startDate {
    NSDateComponents *offsetDateComponents = [[NSDateComponents alloc] init];
    
    offsetDateComponents.year = -2;
    
    NSDate *twoYearsBeforeDate = [[NSCalendar currentCalendar]dateByAddingComponents:offsetDateComponents
                                                                                 toDate:[NSDate date]
                                                                                options:0];


    return twoYearsBeforeDate;
}

- (NSDate*)endDate {
    NSDateComponents *offsetDateComponents = [[NSDateComponents alloc] init];
    
    offsetDateComponents.year = 1;
    
    NSDate *yearLaterDate = [[NSCalendar currentCalendar] dateByAddingComponents:offsetDateComponents
                                                                          toDate:[NSDate date]
                                                                         options:0];
    
    return yearLaterDate;
}

#pragma mark - KDCalendarDelegate

-(void)calendarController:(KDCalendarView*)calendarViewController didSelectDay:(NSDate*)date aEvent:(NSDictionary *)event {
    
    NSNumber *eventId = event[@"id"];
    self.detailLabel.text = (eventId)? [NSString stringWithFormat:@"id:%@", eventId]: @"";
}


-(void)calendarController:(KDCalendarView*)calendarViewController didScrollToMonth:(NSDate*)date {
    
    NSDateFormatter* headerFormatter = [[NSDateFormatter alloc] init];
    headerFormatter.dateFormat = @"MMMM, yyyy";
    self.monthDisplay.text = [headerFormatter stringFromDate:date];
    
    //[self enableButtons:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickedPrevious:(id)sender {
    [self stepMonthInCalendarByValue:-1 animation:YES];
}

- (IBAction)clickedNext:(id)sender {
    [self stepMonthInCalendarByValue:1 animation:YES];
}

- (void)stepMonthInCalendarByValue:(NSInteger)value animation:(BOOL)animation {
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents* oneMonthAheadDateComponents = [[NSDateComponents alloc] init];
    if(value > 12) {
        oneMonthAheadDateComponents.year = round(value/12);
        oneMonthAheadDateComponents.month = (value % 12);
    } else {
        oneMonthAheadDateComponents.month = value;
    }
    NSDate* monthDisplayed = self.calendarView.monthDisplayed;
    NSDate* oneMonthLaterDate = [calendar dateByAddingComponents:oneMonthAheadDateComponents toDate:monthDisplayed options:0];
    
    [self.calendarView setMonthDisplayed:oneMonthLaterDate animated:animation];
}
@end
