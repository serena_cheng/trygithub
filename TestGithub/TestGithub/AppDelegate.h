//
//  AppDelegate.h
//  TestGithub
//
//  Created by serena_cheng on 10/21/15.
//  Copyright (c) 2015 serena_cheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

