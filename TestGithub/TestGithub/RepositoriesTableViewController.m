//
//  RepositoriesTableViewController.m
//  TestGithub
//
//  Created by serena_cheng on 10/21/15.
//  Copyright (c) 2015 serena_cheng. All rights reserved.
//

#import "RepositoriesTableViewController.h"
#import "ActivitiesViewController.h"

#define GITHUB_ORG_REPO_API @"https://api.github.com/orgs/%@/repos"

@interface RepositoriesTableViewController ()

@property (nonatomic, strong) NSArray *repos;

@end

@implementation RepositoriesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = self.organization;
    [self getReposOfOrganization:self.organization];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getReposOfOrganization:(NSString *)organization {

    NSString *api = [NSString stringWithFormat:GITHUB_ORG_REPO_API, organization];
    
    NSURL *url = [NSURL URLWithString:api];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20];
    
    urlRequest.HTTPMethod = @"Get";
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        NSInteger responseStatusCode = [httpResponse statusCode];
        NSLog(@"Status Code: %ld", (long)responseStatusCode);
        
        if([data length] > 0 && connectionError == nil && responseStatusCode == 200) {
            self.repos = [NSJSONSerialization JSONObjectWithData:data
                                                         options:0
                                                           error:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
            
        } else if([data length] == 0 && connectionError == nil) {
            NSLog(@"No data.");
        } else if(connectionError != nil) {
            NSLog(@"Connection failed.");
        }
    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.repos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* const repoCellIdentifier = @"RepositoryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:repoCellIdentifier forIndexPath:indexPath];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:repoCellIdentifier];
    }

    cell.textLabel.font = [UIFont systemFontOfSize:17.0f];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0f];
    
    cell.textLabel.text = self.repos[indexPath.row][@"name"];
    cell.detailTextLabel.text = self.repos[indexPath.row][@"pushed_at"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *repo = self.repos[indexPath.row];
    [self performSegueWithIdentifier:@"showEvents" sender:repo];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation
*/
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"showEvents"]) {
        ((ActivitiesViewController *)[segue destinationViewController]).repoInfo = sender;
    }

}


@end
