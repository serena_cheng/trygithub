//
//  OrganizationViewController.h
//  TestGithub
//
//  Created by serena_cheng on 10/21/15.
//  Copyright (c) 2015 serena_cheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrganizationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *organizationTextField;

- (IBAction)clickedButton:(id)sender;

@end
