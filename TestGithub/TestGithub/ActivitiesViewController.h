//
//  ActivitiesViewController.h
//  TestGithub
//
//  Created by serena_cheng on 10/21/15.
//  Copyright (c) 2015 serena_cheng. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KDCalendarView.h"


@interface ActivitiesViewController : UIViewController <KDCalendarDelegate, KDCalendarDataSource>

@property (strong, nonatomic) IBOutlet KDCalendarView *calendarView;
@property (weak, nonatomic) IBOutlet UILabel *monthDisplay;
@property (nonatomic, strong) NSDictionary *repoInfo;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
- (IBAction)clickedPrevious:(id)sender;
- (IBAction)clickedNext:(id)sender;

@end
